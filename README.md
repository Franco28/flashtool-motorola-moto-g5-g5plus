
#** FlashTool-Motorola v1.0 **#
##by Franco28##

##**FEATURES**##

**Flash Stock firmwares!**
**Flash logo!**
**Flash recovery!**

##**USAGE:**##

1. Run "FlashTool-Motorola" from it's location in terminal 
2. Enjoy! 

##**EXAMPLE:**##

cd /home/user/location/
in terminal type this --- ./FlashTool-Motorola

**OR**

Double-click the FlashTool-Motorola file and choose "Run in Terminal" if your OS supports it.

##**DEPENDENCIES:**##

* adb + fastboot + mfastboot ----> Included!
